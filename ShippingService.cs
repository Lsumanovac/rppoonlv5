﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPPOON_LV5
{
    class ShippingService
    {
        private double pricePerUnitOfMass;

        public ShippingService(double pricePerUnitOfMass)
        {
            this.pricePerUnitOfMass = pricePerUnitOfMass;
        }

        public double DeliveryPrice(double weight)
        {
            return weight * pricePerUnitOfMass;
        }
    }
}
