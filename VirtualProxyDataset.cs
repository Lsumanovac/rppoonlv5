﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPPOON_LV5
{
    class VirtualProxyDataset : IDataset
    {
        private string filePath;
        private DataSet dataset;

        public VirtualProxyDataset(string filePath)
        {
            this.filePath = filePath;
        }

        public ReadOnlyCollection<List<string>> GetData()
        {
            if(dataset == null)
            {
                dataset = new DataSet(filePath);
            }
            return dataset.GetData();
        }
    }
}
