using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPPOON_LV5
{
    class Program
    {
        static void Main(string[] args)
        {
            //Main12();
            Main3();
        }

        static void Main12() //Main za 1. i 2. zadatak
        {
            Box box = new Box("Box Name");
            Product product1 = new Product("Product Name1", 4.99, 2);
            Product product2 = new Product("Product Name2", 11.99, 5);

            box.Add(product1);
            box.Add(product2);

            ShippingService delivery = new ShippingService(10);

            Console.WriteLine("Shipment will cost " + delivery.DeliveryPrice(box.Weight) + " euros.");
        }

        static void Main3() //Main za 3. zadatak
        {
            DataSet data1 = new DataSet("E:\\RPPOONLV5Zad3.txt");
            User user1 = User.GenerateUser("Username1");
            User user2 = User.GenerateUser("Username2");
            User user3 = User.GenerateUser("Username3");
            ProtectionProxyDataset proxy1 = new ProtectionProxyDataset(user1);
            ProtectionProxyDataset proxy2 = new ProtectionProxyDataset(user2);
            ProtectionProxyDataset proxy3 = new ProtectionProxyDataset(user3);

            VirtualProxyDataset virtualProxy = new VirtualProxyDataset("E:\\RPPOONLV5Zad3.txt");
            DataConsolePrinter printer = new DataConsolePrinter();
            printer.Print(proxy1);
            Console.WriteLine();
            printer.Print(proxy2);
            Console.WriteLine();
            printer.Print(proxy3);
            Console.WriteLine("Virtual:");
            printer.Print(virtualProxy);
        }


    }
}
